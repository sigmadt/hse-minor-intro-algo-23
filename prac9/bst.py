
class Node:
    def __init__(self, key):
        self.left = None
        self.right = None
        self.val = key


class BST:
    def __init__(self):
        self.root = None

    def insert(self, key):
        if self.root is None:
            self.root = Node(key)
        else:
            self.insert_recursive(self.root, key)

    def insert_recursive(self, node, key):
        if key < node.val:
            if node.left is None:
                node.left = Node(key)
            else:
                self.insert_recursive(node.left, key)
        else:
            if node.right is None:
                node.right = Node(key)
            else:
                self.insert_recursive(node.right, key)

    def search(self, key):
        return self.search_recursive(self.root, key)

    def search_recursive(self, node, key):
        if node is None:
            return False
        if node.val == key:
            return True

        if key < node.val:
            return self.search_recursive(node.left, key)
        else:
            return self.search_recursive(node.right, key)


if __name__ == '__main__':
    bst = BST()

    bst.insert(8)
    bst.insert(3)
    bst.insert(1)
    bst.insert(6)
    bst.insert(4)
    bst.insert(7)
    bst.insert(10)
    bst.insert(14)
    bst.insert(13)

    print(bst.search(10))
    print(bst.search(7))
    print(bst.search(1))
    print(bst.search(-100))
    print(bst.search(10.1))


