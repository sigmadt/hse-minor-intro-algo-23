import heapq


def heap_sort(h):
    heapq.heapify(h)
    N = len(h)

    return [heapq.heappop(h) for _ in range(N)]


if __name__ == '__main__':
    arr = [3, 1, 4, 1, 5, 9, 2, 6]
    #           1
    #    1               2
    #  3  5           9    4
    # 6
    #

    print(heap_sort(arr))




