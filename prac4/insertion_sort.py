def insertion_sort(arr: list):
    n = len(arr)
    
    for i in range(1, n):
        print(f"{i}-th pos, arr before swap: {arr}")
        curr = arr[i]
        pos = i - 1

        # пытаемся найти позицию, после которой нужно вставить curr
        while pos >= 0 and curr < arr[pos]:
            arr[pos + 1] = arr[pos]
            pos -= 1

        arr[pos + 1] = curr
        print(f"{i}-th pos, arr before swap: {arr}")


if __name__ == '__main__':
    my_arr = [17, 2, 21, 34, 4, 1, 3, 10]

    insertion_sort(my_arr)

    print(my_arr)
