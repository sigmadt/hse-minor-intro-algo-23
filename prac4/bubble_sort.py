def bubble_sort(arr: list):
    n = len(arr)

    for i in range(n):
        print(f"{i}-th pos, arr before swap: {arr}")
        for j in range(0, n - i - 1):
            # если стоят неправильно
            if arr[j] > arr[j + 1]:
                arr[j], arr[j + 1] = arr[j + 1], arr[j]
        print(f"{i}-th pos, arr before swap: {arr}")


if __name__ == '__main__':
    my_arr = [17, 2, 21, 34, 4, 1, 3, 10]

    bubble_sort(my_arr)

    print(my_arr)
