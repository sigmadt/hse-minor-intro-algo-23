def selection_sort(arr: list):
    n = len(arr)

    for i in range(n):
        print(f"{i}-th pos, arr before swap: {arr}")
        min_idx = i
        for j in range(i + 1, n):
            if arr[j] < arr[min_idx]:
                min_idx = j
        arr[i], arr[min_idx] = arr[min_idx], arr[i]

        print(f"{i}-th pos, arr after swap : {arr}")


if __name__ == '__main__':
    my_arr = [17, 2, 21, 34, 4, 1, 3, 10]

    selection_sort(my_arr)

    print(my_arr)
