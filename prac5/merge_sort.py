def merge(left: list, right: list) -> list:
    merged = []
    left_idx, right_idx = 0, 0

    while left_idx < len(left) and right_idx < len(right):
        if left[left_idx] < right[right_idx]:
            merged.append(left[left_idx])
            # сдвигаем стрелочку
            left_idx += 1
        else:
            merged.append(right[right_idx])
            # сдвигаем стрелочку
            right_idx += 1

    # добавить хвостики
    merged.extend(left[left_idx:])
    merged.extend(right[right_idx:])

    return sorted(merged)


def merge_sort(arr):
    # уже отсортирован
    if len(arr) <= 1:
        return arr

    # делим список на 2 части
    mid = len(arr) // 2
    left = arr[:mid]
    right = arr[mid:]

    left_sorted = merge_sort(left)
    right_sorted = merge_sort(right)

    # слить 2 отсортированных списка
    print(f"got left: {left_sorted}, got right: {right_sorted}")
    return merge(left_sorted, right_sorted)


if __name__ == '__main__':
    a = [2, 8, 5, 3, 9, 4, 1, 7]

    print(merge_sort(a))

