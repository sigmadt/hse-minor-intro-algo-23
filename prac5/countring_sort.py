def counting_sort(arr, min_val, max_val) -> list:
    result = []
    count_dict = {}

    # складываем в словарь сколько раз встретили каждый элемент
    for num in arr:
        if num in count_dict:
            count_dict[num] += 1
        else:
            count_dict[num] = 1

    for i in range(min_val, max_val + 1):
        if i in count_dict:
            # сколько раз встретили в массиве
            curr_cnt = count_dict[i]
            result.extend([i for _ in range(curr_cnt)])

    return result


if __name__ == '__main__':
    a = [3, 3, 5, 3, 2, 7, 3, 2, 4, 5, 7, 3]

    print(counting_sort(a, 1,  10))
