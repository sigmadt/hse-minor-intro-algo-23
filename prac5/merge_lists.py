def merge(left: list, right: list) -> list:
    merged = []
    left_idx, right_idx = 0, 0

    while left_idx < len(left) and right_idx < len(right):
        if left[left_idx] < right[right_idx]:
            merged.append(left[left_idx])
            # сдвигаем стрелочку
            left_idx += 1
        else:
            merged.append(right[right_idx])
            # сдвигаем стрелочку
            right_idx += 1

    # добавить хвостики
    merged.extend(left[left_idx:])
    merged.extend(right[right_idx:])

    return sorted(merged)


if __name__ == '__main__':
    a1 = [2, 5, 7, 61, 100, 120, 345]
    a2 = [8, 12, 44, 50]

    print(merge(a1, a2))
