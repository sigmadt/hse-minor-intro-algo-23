def fibonacci_recursive(n: int) -> int:
    # print(f"GOT number {n}", end=" ")
    if n <= 1:
        return n
    return fibonacci_recursive(n - 1) + fibonacci_recursive(n - 2)


def fibonacci_memo(n: int, memo={}) -> int:
    # если уже посчитали
    if n in memo:
        return memo[n]
    # если 0 или 1
    if n <= 1:
        return n
    memo[n] = fibonacci_memo(n - 1, memo) + fibonacci_memo(n - 2, memo)
    return memo[n]


def fibonacci_iterative(n: int) -> int:
    if n <= 1:
        return n
    fib_table = [0 for _ in range(n + 1)]
    fib_table[0] = 0
    fib_table[1] = 1
    for i in range(2, n + 1):
        fib_table[i] = fib_table[i - 1] + fib_table[i - 2]
    return fib_table[n]


if __name__ == '__main__':
    # F(n) = F(n - 1) + F(n - 2)
    N = 200
    # print(fibonacci_recursive(N))
    # print(fibonacci_memo(N))
    print(fibonacci_iterative(N))
