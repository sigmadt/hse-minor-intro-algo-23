def count_ways(coins, target):
    dp = [0 for _ in range(target + 1)]

    dp[0] = 1

    for coin in coins:
        for amount in range(coin, target + 1):
            dp[amount] = dp[amount] + dp[amount - coin]
            print(f"COIN: {coin}")
            print(dp)

    return dp[target]


if __name__ == '__main__':
    coins = [1, 2, 5]
    target = 5
    # 5 = 1 + 1 + 1 + 1 + 1
    # 5 = 2 + 1 + 1 + 1
    # 5 = 2 + 2 + 1
    # 5 = 5
    print(f"Number of ways for coins {coins} to get target {target} is {count_ways(coins, target)}")
