def is_palindrome(input_string):
    stack = []

    middle_pos = len(input_string) // 2

    for char in input_string[:middle_pos]:
        stack.append(char)

    for char in input_string[middle_pos:]:
        top_char = stack.pop()
        print("Достали: ", top_char)
        if char != top_char:
            print("Это не палиндром :(")
            return False

    print("Ура, это палиндром!")
    return True


if __name__ == '__main__':
    # Примеры использования
    # string1 = "А роза упала на лапу Азора"
    palindrome1 = "ШаллаШ"
    string2 = "helloo"

    is_palindrome(string2)

    # print(is_palindrome(string1))  # Выводит True
    # print(is_palindrome(string2))  # Выводит False
