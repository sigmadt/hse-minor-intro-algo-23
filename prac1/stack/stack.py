stack = []

# Положим элементы на стэк

stack.append("green cookie")
stack.append("pink cookie")
stack.append("blue cookie")
stack.append("orange cookie")
stack.append("yellow cookie")

print(f"Банка до: {stack}")

# Достанем верхнее печенье
top_cookie = stack.pop()
print(top_cookie)
print(f"Банка после: {stack}")

stack.pop()
stack.pop()
stack.pop()

print(f"В банке осталось одно вкуснейшее печенье {stack}")
stack.pop()

if not stack:
    print(f"Действительно, банка пуста")
