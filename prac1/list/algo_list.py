def solution_sum(given_numbers):
    # Инициализация переменной для хранения суммы
    total_sum = 0

    # Итерация по массиву и добавление каждого элемента к сумме
    for number in given_numbers:
        total_sum += number

    # Вывод результата
    print("Сумма всех элементов массива:", total_sum)


def solution_max(given_numbers):
    if len(given_numbers) < 1:
        raise ValueError
    curr_max = given_numbers[0]

    # 0, ..., N - 1:  всего N раз
    for el in given_numbers:
        curr_max = max(curr_max, el)

    print(f"Максимальный элемент: {curr_max}")


# def solution_max_and_min(given_numbers):
#     if len(given_numbers) < 1:
#         raise ValueError
#     curr_max = given_numbers[0]
#     curr_min = given_numbers[0]
#     sum = 0
#     numbers = [x for x in range(len(given_numbers))]
#
#     for el in given_numbers:
#         curr_max = max(curr_max, el)
#         for el in given_numbers
#
#
#     print(f"Максимальный элемент: {curr_max}")
#     print(f"Минимальный элемент: {curr_min}")


def solution_max_and_min_sorted(given_numbers):
    if len(given_numbers) < 1:
        raise ValueError

    print(f"Максимальный элемент: {given_numbers[-1]}")
    print(f"Минимальный элемент: {given_numbers[0]}")


def o_notation(given_numbers):
    numbers = [x for x in range(len(given_numbers))]

    total_sum = 0
    # 0, ..., N - 1:  всего N раз
    for el in given_numbers:
        curr_sum = 0
        # 0, ... N - 1: всего N раз
        for new_el in numbers:
            curr_sum += new_el
        total_sum += (curr_sum + el)

    # Асимптотика: O(N^2)

    print(f"Сумма: {total_sum}")


def calc_len(lst):
    # Алгоритм 1:
    # Асимптотика: O(N)
    curr_len = 0

    for el in lst:
        curr_len += 1

    print(f"Длина списка {curr_len}")

    # Алгоритм 2:
    # Асимптотика O(1)
    print(f"Длина списка {len(lst)}")


if __name__ == '__main__':
    # Заданный массив
    numbers = [100, -1, 23, 44, 989, -71129, 4, 66, 75]

    # 1. Задача найти сумму элементов
    # Асимптотика: O(N)
    # solution_sum(numbers)

    # 2. Задача найти максимальный элемент в массиве
    # Асимптотика: O(N)
    # solution_max(numbers)

    # 3. Задача найти максимальный и минимальный элемент в массиве
    # Асимптотика
    # solution_max_and_min(numbers)

    # 4*. Задача найти максимальный и минимальный элемент в
    # отсортированном по возрастанию массиве
    # Асимптотика O(1)
    sorted_lst = [x + 99 for x in range(180, 1000)]
    solution_max_and_min_sorted(sorted_lst)
