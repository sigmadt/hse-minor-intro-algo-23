lst = []

lst.append('a')
# Внутри Python длина увеличилась на 1
lst.append('d')
lst.append('x')
lst.append('y')

lst.insert(2, 'm')

lst.remove('y')
# Длина уменьшилась на 1


N = 1000000000
# long_lst = [x + 1 for x in range(N)]
# [1, 2, 3, 4, ..., 100]

# 1 Подход: сложить все числа через цикл
total_sum = 0

# Асимптотика: O(N)
# for el in long_lst:
#     sum += el
#
# print(total_sum)

# 2 Подход: воспользоваться формулой для суммы арифметической прогрессии
# sum = N * (N + 1) / 2
# Асимптотика: O(1)
print(f"Количество элементов в массиве: {N}")
print(int(N * (N + 1) / 2))
