from collections import deque


def first_uniq_char(s):
    char_count = {}
    char_queue = deque()

    for char in s:
        char_count[char] = char_count.get(char, 0) + 1
        char_queue.append(char)

    while char_queue:
        char = char_queue.popleft()
        if char_count[char] == 1:
            return s.index(char)

    return -1


if __name__ == '__main__':
    s = "HHSEEE"
    result = first_uniq_char(s)
    print(result) 
