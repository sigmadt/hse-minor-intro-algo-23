from itertools import permutations


def count_perms_with_constraint(s, constraint):
    perms = permutations(s)
    result = []

    for p in perms:
        if constraint not in "".join(p):
            result.append(p)

    return result


if __name__ == '__main__':
    friends = "ВДАС"
    for p in count_perms_with_constraint(friends, "АДВ"):
        print(p)
