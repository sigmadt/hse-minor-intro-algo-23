import itertools


def count_subsets(s: set) -> int:
    n = len(s)
    return 2 ** n


def count_subsets_brute_force(s: set) -> int:
    n = len(s)
    subsets = []
    for i in range(n + 1):
        for sb in itertools.combinations(s, i):
            print(sb)
            subsets.append(sb)

    return len(subsets)


if __name__ == '__main__':
    friends = {"Ваня", "Даня", "Аня"}
    # print(f"Number of all subsets of set {friends} is {count_subsets(friends)}")

    print(f"Number of all subsets of set {friends} is {count_subsets_brute_force(friends)}")