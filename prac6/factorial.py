def factorial_iterative(n: int) -> int:
    if n < 0:
        return -1

    result = 1
    for i in range(1, n + 1):
        result = result * i

    return result


def factorial_recursive(n: int) -> int:
    if n < 0:
        return -1
    if n <= 1:
        return 1
    # N! = (N - 1)! * N
    return factorial_iterative(n - 1) * n


def factorial_memo(n: int, memo=None) -> int:
    if memo is None:
        memo = {}
    if n < 0:
        return -1
    if n in memo:
        return memo[n]
    if n <= 1:
        return 1
    memo[n] = n * factorial_memo(n - 1, memo)
    return memo[n]


if __name__ == '__main__':
    N = 100
    print(factorial_iterative(N))

    print(factorial_recursive(N))

    print(factorial_memo(N))
