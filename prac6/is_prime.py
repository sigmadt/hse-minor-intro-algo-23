def naive_is_prime(n: int) -> bool:
    for i in range(2, n):
        if n % i == 0:
            return False
    return True


def sqrt_is_prime(n: int) -> bool:
    sqrt_n = int(n**0.5) + 1
    for i in range(2, sqrt_n):
        if n % i == 0:
            return False
    return True


def step_is_prime(n: int) -> bool:
    if n <= 1:
        return False
    if n <= 3:
        return True
    if n % 2 == 0 or n % 3 == 0:
        return False

    # 6k +/- 1
    # 1. 6k - 1 6k + 5 6k + 11
    # 2. 6k + 1 и 6(k+1)-1=6k+5
    sqrt_n = int(n**0.5) + 1
    for i in range(5, sqrt_n, 6):
        if n % i == 0 or n % (i + 2) == 0:
            return False
    return True


def generate_sieve_of_erathosthenes(n: int) -> list:
    # решето, где по индексу мы можем узнать является ли число простым
    sieve = [True for _ in range(n + 1)]
    sieve[0], sieve[1] = False, False

    sqrt_n = int(n**0.5) + 1
    for i in range(2, sqrt_n):
        # if i is prime
        if sieve[i]:
            for j in range(i ** 2, n + 1, i):
                sieve[j] = False

    return [x for x, is_prime in enumerate(sieve) if is_prime]


def sieve_is_prime(n: int) -> bool:
    prime_numbers = generate_sieve_of_erathosthenes(n)
    return n in prime_numbers


if __name__ == '__main__':
    # n = 2147481997
    # print(f"Is {n} prime: {step_is_prime(n)}")
    prime_numbers = [2, 3, 5, 7, 11, 13, 17, 19, 7829]

    for p in prime_numbers:
        is_prime = sieve_is_prime(p)
        if is_prime:
            print(f"Number {p} is prime")
        else:
            print(f"Number {p} is not prime")


    prime_numbers_less_than_7829 = generate_sieve_of_erathosthenes(7829)

    for x, is_p in enumerate(prime_numbers_less_than_7829[:50]):
        print(f"Is {x} prime: {is_p}")
