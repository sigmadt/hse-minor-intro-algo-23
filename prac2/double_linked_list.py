class DoubleListNode:
    def __init__(self, val):
        self.val = val
        self.next = None
        self.prev = None


# dog -> cat
# dog <- cat
animals_list_head = DoubleListNode("dog")
animals_list_tail = DoubleListNode("cat")

animals_list_head.next = animals_list_tail
animals_list_tail.prev = animals_list_head


def pretty_print_next(head: DoubleListNode):
    curr = head
    result = []
    while curr:
        result.append(curr.val)
        curr = curr.next
    print(" <-> ".join([str(x) for x in result]))


def pretty_print_prev(head: DoubleListNode):
    curr = head
    result = []
    while curr:
        result.append(curr.val)
        curr = curr.prev
    print(" <-> ".join([str(x) for x in result]))


pretty_print_next(animals_list_head)
pretty_print_prev(animals_list_tail)
