class ListNode:
    def __init__(self, val):
        self.val = val
        self.next = None


# Создание односвязного списка
linked_list_head = ListNode(17)
linked_list_head.next = ListNode(100)
linked_list_head.next.next = ListNode(21)


# 17 -> 100 -> 21
def pretty_print(head: ListNode):
    # Time Complexity
    # Асимптотика: O(N)
    curr = head
    result = []

    while curr:
        result.append(curr.val)
        print(curr.val, end=" -> ")
        curr = curr.next

    print(" -> ".join([str(x) for x in result]))


# 17 -> 100 -> 21 -> 44
def append(head: ListNode, new_val):
    # Time Complexity
    # Асимптотика: O(N)
    curr = head
    while curr.next:
        curr = curr.next

    curr.next = ListNode(new_val)


def size(head: ListNode) -> int:
    # Time Complexity
    # Асимптотика: O(N)
    curr_len = 0
    curr = head
    while curr:
        curr_len += 1
        curr = curr.next

    return curr_len


def contains(head: ListNode, val) -> bool:
    # Time Complexity
    # Асимптотика: O(N)
    curr = head
    while curr:
        if curr.val == val:
            return True
        curr = curr.next

    return False


names_list = ListNode("Dmitry")
append(names_list, "Mary")
append(names_list, "Nastya")
append(names_list, "Danya")

print(contains(names_list, "Nastya"))
print(contains(names_list, "Pasha"))


# Dmitry -> Mary -> Nastya -> Danya
def remove(head: ListNode, val):
    curr = head

    while curr and curr.next:
        if curr.next.val != val:
            curr = curr.next
        else:
            curr.next = curr.next.next


# Dmitry -> Mary -> Danya
remove(names_list, "Nastya")
pretty_print(names_list)
