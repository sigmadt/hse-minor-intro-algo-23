def binary_search(arr: list, target: int) -> int:
    left, right = 0, len(arr) - 1
    it = 1
    while left <= right:
        print(f"Iter N{it}")
        mid_pos = (left + right) // 2
        if arr[mid_pos] == target:
            return mid_pos
        elif arr[mid_pos] < target:
            left = mid_pos + 1
        else:
            right = mid_pos - 1
        it += 1

    # мы не нашли элемент, поэтому возвращаем позицию -1
    return -1


if __name__ == '__main__':
    # arr = [1, 3, 15, 21, 44, 67, 71, 85, 98]
    # target = 15
    # print(f"Position of {target}: {binary_search(arr, target)}")

    # arr = [1, 3, 15, 21, 44, 67, 71, 85, 98]
    # target = 49
    # print(f"Position of {target}: {binary_search(arr, target)}")

    arr = [x + 2 for x in range(10, 1000000)]
    target = 100000 - 3
    print(f"Position of {target}: {binary_search(arr, target)}")
