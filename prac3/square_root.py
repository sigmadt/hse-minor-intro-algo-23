def square_root(K, precision=0.01):
    if K < 0:
        return None
    if K <= 1:
        return K

    # ---- 1 -- ... -- K ----->
    left, right = 0, K
    ans = 0

    while right - left > precision:
        mid = (right + left) / 2.0
        if mid * mid == K:
            return mid

        elif mid * mid < K:
            left = mid
            ans = mid
        else:
            right = mid

    return round(ans, 2)


if __name__ == '__main__':
    K = 2
    print(f"Square root of {K} is {square_root(K)}")








