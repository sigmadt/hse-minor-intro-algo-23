# Практики по майнору Алгоритмы (осень 2023)
Здесь вы найдете оглавление по практикам.

## 1. Массив, стек, очередь.
Ссылка на [исходный код](https://gitlab.com/sigmadt/hse-minor-intro-algo-23/-/tree/main/prac1?ref_type=heads).


## 2. Односвязный и двусвязный списки.
Ссылка на [исходный код](https://gitlab.com/sigmadt/hse-minor-intro-algo-23/-/tree/main/prac2?ref_type=heads).


## 3. Бинарный поиск.
Ссылка на [исходный код](https://gitlab.com/sigmadt/hse-minor-intro-algo-23/-/tree/main/prac3?ref_type=heads).

## 4. Квадратичные сортировки.
Ссылка на [исходный код](https://gitlab.com/sigmadt/hse-minor-intro-algo-23/-/tree/main/prac4?ref_type=heads).

## 5. Продвинутые сортировки.
Ссылка на [исходный код](https://gitlab.com/sigmadt/hse-minor-intro-algo-23/-/tree/main/prac5?ref_type=heads).

## 6. Арифметические алгоритмы.
Ссылка на [исходный код](https://gitlab.com/sigmadt/hse-minor-intro-algo-23/-/tree/main/prac6?ref_type=heads).

## 7. Демонические программирование.
Ссылка на [исходный код](https://gitlab.com/sigmadt/hse-minor-intro-algo-23/-/tree/main/prac7?ref_type=heads).

## 8. Комбинаторика и перебор.
Ссылка на [исходный код](https://gitlab.com/sigmadt/hse-minor-intro-algo-23/-/tree/main/prac8?ref_type=heads).

## 9. Кучи и деревья поиска.
Ссылка на [исходный код](https://gitlab.com/sigmadt/hse-minor-intro-algo-23/-/tree/main/prac9?ref_type=heads).

## 10. Регулярные выражения.
Ссылка на [исходный код](https://gitlab.com/sigmadt/hse-minor-intro-algo-23/-/tree/main/prac10?ref_type=heads).

## 11. Алгоритмы на строках.
Ссылка на [исходный код](https://gitlab.com/sigmadt/hse-minor-intro-algo-23/-/tree/main/prac11?ref_type=heads).
